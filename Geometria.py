#Tarea 4
#EJERCICIO:
#Crear un programa en Python que utilice la librería pygame
#y todo el código de la "Tarea 3"  para dibujar un plano
#cartesiano y representar gráficamente los objetos Punto
#y los objetos Rectángulo creados en la tarea anterior.
#La salida del programa debe ser capturada en una imagen
#(similar a la mostrada al final) y guardada en el mismo repositorio gitlab de esta tarea.
#AUTHOR:VICTOR GUAMAN


import math


class Punto:

    def __init__(self, x = 0, y = 0):
        self.x = x
        self.y = y

    def cuadrante(self):
        if self.x > 0 and self.y > 0:
            return 'I'
        elif self.x < 0 and self.y > 0:
            return 'II'
        elif self.x < 0 and self.y < 0:
            return 'III'
        elif self.x > 0 and self.y < 0:
             return 'IV'
        elif self.x != 0 and self.y == 0:
             return "{} se encuentra sobre el eje X"
        elif self.x == 0 and self.y != 0:
            return "{} se encuentra sobre el eje Y"
        else:
             return "sobre el origen"

        return self.x and self.y

    def vector(self, p):
        vector = Punto(p.x - self.x, p.y - self.y)
        return vector

    def distancia(self, p):
        distancia = math.sqrt( (p.x - self.x)**2 + (p.y - self.y)**2 )
        return distancia

    def __str__(self):
        return "({}, {})".format(self.x, self.y)


class Rectangulo:
    punto_inicial= None
    punto_final= None

    def __init__(self, punto_inicial, punto_final):
        self.punto_inicial = punto_inicial
        self.punto_final = punto_final

    def base(self):
        return self.punto_final.x - self.punto_inicial.x

    def altura(self):
        return self.punto_final.y - self.punto_inicial.y

    def area(self):
        return  self.base()*self.altura()


if __name__ == '__main__':

    A = Punto(2, 3)
    B = Punto(5, 5)
    C = Punto(-3, -1)
    D = Punto(0, 0)

    print (f"El punto {A} se encuentra en el cuadrante {A.cuadrante()}")
    print (f"El punto {C} se encuentra en el cuadrante {C.cuadrante()}")
    print (f"El punto {D} se encuentra {D.cuadrante()}")
    print (f"La distancia entre el punto {A} y {B} es {A.distancia(B)}")
    print (f"La distancia entre el punto {B} y {A} es {B.distancia(A)}")

    da= A.distancia(D)
    db= B.distancia(D)
    dc= C.distancia(D)
    if da > db and da > dc:
        print (f"El punto {A} se encuentra más lejos del origen")
    elif db > da and db > dc:
        print(f"El punto {B} se encuentra más lejos del origen")
    else:
        print(f"El punto {C} se encuentra más lejos del origen")

    rect = Rectangulo(A, B)
    print("La base del rectangulo es {}".format(rect.base()))
    print("La altura del rectangulo es {}".format(rect.altura()))
    print("El area del rectangulo es {}".format(rect.area ()))

#dibujo la pantalla
import pygame, sys

from pygame.locals import *

pygame.init

ventana=pygame.display.set_mode((500,400))

pygame.display.set_caption("figuras con pygame")

#dibujo las rectas

colorfondo=(25,150,70)
color=(250,80,40)
AZUL=(10.10,100)
while True:
    ventana.fill(colorfondo)
    #linea
    #pygame.draw.line(ventana,color,(30,40),(140,100),20)
    pygame.draw.line(ventana, color, (5, 50), (300, 50), 1) #es la linea de x
    pygame.draw.line(ventana, color, (20, 15), (300, 90000), 1) #es la linea de ye 9000 perntenea a ir bajando  desde la derecha de la y

    pygame.draw.line(ventana, color, (250, 23), (250, 350), 1)
    pygame.draw.line(ventana, color, (90000,0 ), (0, 300), 1)

    pygame.draw.line(ventana, color, (15, 100), (45, 100), 1)  #lineas de corte en el eje y

    pygame.draw.line(ventana, color, (15, 150), (45, 150), 1) #segunda lonea que  corta el eje   y



    pygame.draw.line(ventana, color, (50, 40), (50, 70), 1)




    pygame.draw.line(ventana, color, (100, 40), (100, 70), 1)



    for evento in pygame.event.get():
        if evento.type==QUIT:
            pygame.quit()
            sys.exit()
    pygame.display.update()




